package com.gmail.trukhan.andr.entity;

/**
 * Class Computer describes model of computer in the game "Guess the Number".
 * @author Trukhan Andrei
 */
public class Computer {
    /**
     * This field contains information about the number requested by computer.
     */
    private int computerNumber;

    public void setComputerNumber(int computerNumber) {
        this.computerNumber = computerNumber;
    }

    public int getComputerNumber() {
        return computerNumber;
    }
}
