package com.gmail.trukhan.andr.entity;

import com.gmail.trukhan.andr.message.Message;
import com.gmail.trukhan.andr.service.ConsoleService;
import com.gmail.trukhan.andr.service.GameService;

import java.io.IOException;

/**
 * Class Menu describes model of the main menu of the game "Guess the Number".
 * @author Trukhan Andrei
 */
public class Menu {
    private GameService gameService = new GameService();
    private ConsoleService consoleService = new ConsoleService();

    /**
     * This field contains information about number of menu chosen by user.
     */
    private int taskNumber;

    /**
     * This method describes logic of the menu of the game
     * and processing the choice of the user.
     * @see GameService
     *
     * @throws – IOException in case get not number 1-3.
     */
    public void startMenu() throws IOException {
        consoleService.showMessage(Message.GAME_NAME + Message.GAME_MENU);
        taskNumber = consoleService.getNumberFromConsole();

        switch (taskNumber){
            case 1:
                gameService.startGame();
                startMenu();
                break;
            case 2:
                gameService.showHistory();
                startMenu();
                break;
            case 3:
                gameService.exitGame();
            default:
                consoleService.showEror("Invalid Format! Please, use the numbers 1-3");
                startMenu();
        }
    }
}
