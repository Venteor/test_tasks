package com.gmail.trukhan.andr.entity;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class User describes model of history in the game "Guess the Number".
 * @author Trukhan Andrei
 */
public class History {
    /**
     * This field contains information about the history of games.
     * Max saved games - 3.
     * Use the LRU to sort the results.
     */
    private  Map<Integer, String> historyMap = new LinkedHashMap<>(3, (float) .75, true);

    /**
     * This method describes logic saving result of the game in historyMap.
     *
     * @param bucket - key for map
     * @param guessingResult - result of the game - contains attempts and result(success/failure)
     */
    public void saveHistory(int bucket, String guessingResult){
        historyMap.put(bucket, guessingResult);
    }

    /**
     * This method check is any history of games empty.
     *
     * @return true - history is empty.
     */
    public boolean isEmpty() {
        return historyMap.isEmpty();
    }

    public  Map<Integer, String> getHistory() {
        return historyMap;
    }
}
