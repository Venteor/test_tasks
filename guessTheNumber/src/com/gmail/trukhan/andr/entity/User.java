package com.gmail.trukhan.andr.entity;

/**
 * Class User describes model of user in the game "Guess the Number".
 * @author Trukhan Andrei
 */
public class User {
    /**
     * This field contains information about the number requested by user.
     */
    private int userNumber;

    public int getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(int userNumber) {
        this.userNumber = userNumber;
    }
}
