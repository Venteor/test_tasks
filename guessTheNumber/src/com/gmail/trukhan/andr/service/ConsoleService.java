package com.gmail.trukhan.andr.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class ConsoleService contains methods to get and show messages.
 * @author Trukhan Andrei
 */
public class ConsoleService {
    private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    /**
     * This field contains number got from console.
     */
    private int number;
    /**
     * This field contains string got from console.
     */
    private String consoleReadLine;

    /**
     * This method describes logic of the getting string
     * from console and trying to parse it to integer.
     *
     * @throws – IOException in case get not integer.
     * @return number - number got from console.
     * @return -1 - in case IOException
     */
    public int getNumberFromConsole() throws IOException {
        consoleReadLine = br.readLine();
        try {
            number = Integer.parseInt(consoleReadLine);
        } catch(NumberFormatException nfe) {
            return -1;
        }
        return number;
    }

    /**
     * This method print message on console.
     *
     * @param message
     */
    public void showMessage(String message){
        System.out.println(message + "\n");
    }
    /**
     * This method print error message on console.
     *
     * @param message
     */
    public void showEror(String message){
        System.err.println(message + "\n");
    }
}

