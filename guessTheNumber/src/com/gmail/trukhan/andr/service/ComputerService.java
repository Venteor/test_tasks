package com.gmail.trukhan.andr.service;

import com.gmail.trukhan.andr.entity.Computer;

import java.util.Random;

/**
 * Class User describes model of user in the game "Guess the Number".
 * @author Trukhan Andrei
 */
class ComputerService {
    private ConsoleService consoleService = new ConsoleService();
    private Computer computer = new Computer();
    /**
     * This field contains information about the number requested by computer.
     */
    private int computerNumber;

    /**
     * This method describes logic of init Computer.
     * Set the number requested by computer using
     * ComputerService.thinkingNumber().
     */
    public void init(){
        computer.setComputerNumber(thinkingNumber());
        computerNumber = computer.getComputerNumber();
    }

    /**
     * This method describes logic of compare user number and computer number.
     *
     * @param userNumber - number requested by user.
     * @param countOfAttempt - number of attempt
     * @return true - numbers equal.
     */
    public boolean checkNumber(int userNumber, int countOfAttempt){
        if (computerNumber == userNumber){
            return true;
        }
        else if(computerNumber > userNumber && countOfAttempt != 3) {
            consoleService.showMessage("Sorry! It is not my number." + " Your guess is too low.");
        }
        else if(computerNumber < userNumber && countOfAttempt != 3) {
            consoleService.showMessage("Sorry! It is not my number. " + "Your guess is too high.");
        }
        return false;
    }

    /**
     * This method describes logic of thinking number by computer.
     * Random is used.
     *
     * @return int in bounds 1-20.
     */
    private int thinkingNumber(){
        Random rand = new Random();
        return rand.ints(1, 21).findFirst().getAsInt();
    }

    public int getComputerNumber() {
        return computerNumber;
    }
}
