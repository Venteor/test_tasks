package com.gmail.trukhan.andr.service;


import com.gmail.trukhan.andr.message.Message;

import java.io.IOException;

/**
 * Class GameService describes main logic of the game "Guess the Number".
 * @author Trukhan Andrei
 */
public class GameService {
    private ConsoleService consoleService = new ConsoleService();
    private ComputerService computerService = new ComputerService();
    private UserService userService = new UserService();
    private HistoryService historyService = new HistoryService();
    /**
     * This field contains information about the number of attempt.
     * Max - 3.
     */
    private int countOfAttempt = 0;
    /**
     * This field contains information about result of the game (success or failure).
     */
    private boolean isSuccess = false;

    /**
     * This method describes logic of the game "Guess the Number".
     *
     * @throws – IOException in case get not integer.
     */
    public void startGame() throws IOException{
        computerService.init();
        consoleService.showMessage("Well, I am thinking a number between 1 and 20.");
        guessNumber();
        if(isSuccess){
            consoleService.showMessage(Message.CONGRATULATIONS + "    Your guess was successful!");
        }
        else {
            consoleService.showMessage(Message.GAME_OVER +
                    "My number was " + computerService.getComputerNumber() + ". Try again in the New Game.");
        }
    }

    /**
     * This method describes logic of attempt to guess computer number by user.
     *
     * @throws – IOException in case get not integer.
     */
    private void guessNumber() throws IOException {
        countOfAttempt += 1;
        int userNumber;
        userNumber = userService.guess();
        historyService.setUserNumbers(userNumber);
        isSuccess = computerService.checkNumber(userNumber, countOfAttempt);
        if(!isSuccess && countOfAttempt < 3){
            consoleService.showMessage("You have " + (3 - countOfAttempt) + " attempt yet.");
            guessNumber();
        }
        else {
            countOfAttempt = 0;
            historyService.saveResult(isSuccess);
        }
    }

    /**
     * This method describes logic of exit the games.
     */
    public void exitGame(){
        consoleService.showMessage(Message.GOODBYE);
        System.exit(0);
    }

    /**
     * This method describes logic of showing the history of the games.
     * @see HistoryService
     */
    public void showHistory(){
        historyService.showHistory();
    }
}
