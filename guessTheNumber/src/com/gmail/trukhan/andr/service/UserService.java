package com.gmail.trukhan.andr.service;

import com.gmail.trukhan.andr.entity.User;

import java.io.IOException;

/**
 * Class UserService contains method to take a guess by user.".
 * @author Trukhan Andrei
 */
class UserService {
    private User user = new User();
    private ConsoleService consoleService = new ConsoleService();

    /**
     * This method describes logic of guess by user.
     * If userNumber get -1, show error message and trying to
     * get the number one more time.
     * @see ConsoleService
     *
     * @throws – IOException in case get not integer.
     * @return int number got from console.
     */
    public int guess() throws IOException {
        consoleService.showMessage("Take a guess.");
        int userNumber = consoleService.getNumberFromConsole();
        if(userNumber == -1){
            consoleService.showEror("Invalid Format! Please, use the number.");
            guess();
        }
        else {
            user.setUserNumber(userNumber);
        }
        return user.getUserNumber();
    }
}
