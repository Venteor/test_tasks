package com.gmail.trukhan.andr.service;

import com.gmail.trukhan.andr.entity.History;
import com.gmail.trukhan.andr.message.Message;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Map;

/**
 * Class User describes model of user in the game "Guess the Number".
 * @author Trukhan Andrei
 */
class HistoryService {
    private ConsoleService consoleService = new ConsoleService();
    private History history = new History();
    /**
     * This field contains information about the history of games.
     */
    private Map<Integer, String> historyMap;
    /**
     * This field contains information about numbers
     * of the guess attempt in active game.
     */
    private ArrayList<Integer> userNumbers = new ArrayList<>();
    /**
     * This field contains information about the key for saving history.
     */
    private int bucket = 0;

    /**
     * This method describes logic saving result of the game in historyMap.
     * Using bucket in bounds 1-3 to exclude the collisions in map.
     * @param isSuccess - is game was success or failure
     */
    public void saveResult(boolean isSuccess) {
        StringBuilder guessingResult = new StringBuilder(); // contains result of the game

        if(bucket == 3) bucket = 0;
        bucket += 1;
        if (isSuccess){
            guessingResult.append(" success ");
        }
        else {
            guessingResult.append(" failure ");
        }
        for(Integer userNumber : userNumbers){
            guessingResult.append(userNumber)
                    .append(" ");
        }
        history.saveHistory(bucket, guessingResult.toString());
        userNumbers.clear();
    }

    /**
     * This method describes logic of showing the history of the games.
     */
    public void showHistory() {
        int count = 0; // the number of row in list of games.
        historyMap = history.getHistory();
        consoleService.showMessage(Message.GAME_HISTORY);
        if(history.isEmpty()){
            consoleService.showMessage("       You don't played yet!");
        }
        ListIterator<String> itr = new ArrayList<>(historyMap.values()).listIterator(historyMap.size());
        while (itr.hasPrevious()) {
            count += 1;
            String value = itr.previous();
            consoleService.showMessage("        " + count + value);
        }
    }

    public void setUserNumbers(int userNumber) {
        userNumbers.add(userNumber);
    }
}
