package com.gmail.trukhan.andr.message;

/**
 * Class Message contains ACII Art for the game "Guess the Number".
 * @author Trukhan Andrei
 */
public class Message {

    public static final String GAME_NAME =
            " +-+-+-+-+-+ +-+-+-+ +-+-+-+-+-+-+\n" +
            " |G|U|E|S|S| |t|h|e| |N|U|M|B|E|R|\n" +
            " +-+-+-+-+-+ +-+-+-+ +-+-+-+-+-+-+\n";

    public static final String GAME_MENU =
            "        Welcome to the game!" + "\n" +
            "===================================" + "\n" +
            "        1. Start a New Game" + "\n" +
            "        2. Show history" + "\n" +
            "        3. Exit the game"+ "\n" + "\n" +
            "Please, select one of the numbers.";

    public static final String CONGRATULATIONS =
            "  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
            "  |C|O|N|G|R|A|T|U|L|A|T|I|O|N|S|\n" +
            "  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n";

    public static final String GAME_OVER =
            "        +-+-+-+-+ +-+-+-+-+\n" +
            "        |G|A|M|E| |O|V|E|R|\n" +
            "        +-+-+-+-+ +-+-+-+-+\n";

    public static final String GOODBYE =
            "        +-+-+-+-+-+-+-+-+\n" +
            "        |G|O|O|D|B|Y|E|!|\n" +
            "        +-+-+-+-+-+-+-+-+";

    public static final String GAME_HISTORY =
            " +-+-+-+-+-+-+-+ +-+-+ +-+-+-+-+-+\n" +
            " |H|I|S|T|O|R|Y| |o|f| |G|A|M|E|S|\n" +
            " +-+-+-+-+-+-+-+ +-+-+ +-+-+-+-+-+";
}
