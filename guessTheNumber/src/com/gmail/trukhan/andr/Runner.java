package com.gmail.trukhan.andr;

import com.gmail.trukhan.andr.entity.Menu;
/**
 * Class Runner run the production of the app".
 * @author Trukhan Andrei
 */
import java.io.IOException;

public class Runner {
    public static void main(String[] args) throws IOException {
        Menu menu = new Menu();
        menu.startMenu();
    }
}
